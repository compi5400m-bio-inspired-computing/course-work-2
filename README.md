# COMP5400 Bio-Inspired Computing Coursework 2

The following repository is a collection of the code developed for the COMP5400M Bio-Inspired Computing module at the University of Leeds. The team members are as follows:

 * Archie Crew-Gee: MEng Mechatronics and Robotics (Ind)
 * Yael May: MSc Mechatronics and Robotics
 * Shilpa Namboodiri: MSc Mechatronics and Robotics


 PLEASE NOTE: The authors of this code and the technical report are Yael and Archie. A theoretical report will be submitted by Shilpa. The overall conclusions from both practical and theoretical observations will be presented during the presentation.

 ## Context - written by Yael

 This project includes an implementation of 'The Game of Life', also known as 'Life', by Conway. This is a form of cellular automaton and is traditionally viewed as a simulation rather than involving the user.

 This code has been submitted alongside a report explaining the results and implementation in more detail.

 ## Goal - written by Yael

 The main aim of this project is to determine whether user interaction with 'The Game of Life' produces any interesting patterns or can influence the animation in a negative or positive way.

 This will be achieved by the user being able to select the squares and editing the rules to their liking.

 The original game of life rules are as follows:

 1. Alive with 1/0 neighbours dies from underpopulation
 2. Alive with 2/3 neighbours survives
 3. Alive with more than three live neighbours dies from overpopulation
 4. Aead cells with three live neighbours becomes alive

These rules are modified in game by changing the values in the right hand side of the screen such that:

 1. Modifies the underpopulation upper value
 2. Modifies the survival upper value
 3. Modifies the death lower value
 4. Modifies the rebirth value

 ## Project Structure and Flow - written by Yael

 This project uses the main.py file which is where the main game functions are called from the Game of Life class (game_of_life.py) via the game.iterate() command. The iterate() function composes of a state machine which controls the programme flow between different screens, including the game modes (user_input_screen or game_screen). This means that the start_screen is loaded first. Once the start button is pressed, on the start_screen, it switches to the user_input_screen. Then depending on the game state or whether the reset button has been pressed by the user, the game will switch between the user_input_screen and the game_screen. All button, textbox and the state machine structure classes are stored in basic_classes.py. The configuration settings such as colours, CSV file name, world and screen sizes are hard coded here to be able to re-use them throughout the project and are imported from config.json.

 The main game screen is split into 20% as the user interface panel and 80% is actually the grid where the simulation can be seen. The grid is constructed from vertical and horizontal lines where the spacing varies depending on how many squares are needed and then the alive cell size is matched to the size of a cell in the grid.

 For each iteration, the generation count increases and the number of 'alive' cells is also counted and outputted as the population count. These values are outputted to a CSV file in case they are needed for analysis. At the start of the game, the old file is overwritten/created, if needed, but for each iteration, it is just appended too.

 Once cells (squares) have been selected by the user, they click the 'Ok' button and this starts the simulation. The user can change the rules at any point by inputting a numerical number between 0 and 8 into any or all of the textboxes. If nothing was inputted, then the original rule is used. On the next iteration, these rules are updated and implemented into the game. If the simulation is about to go off of the screen, then the size of the grid space increases, making the grid squares smaller to accommodate the pattern being produced.

 The user can reset the game and reselect squares by pressing the 'Reset' button. This will take the user back to the user_input_screen, to begin again. The game can be quit by exiting the window as this is a game interrupt. This custom exception alongside invalid states or a lack of game states are imported from the custom_exceptions.py. The button clicks are checked for continuously throughout the game, as is the user input of a mouse over the textboxes to allow the user to input into the textboxes. The rules are only updated, even if there is text in the textboxes, once the 'Ok' button is pressed.

 ## Setup - written by Archie

 To run this code, a clone of the repository is required. Navigating to the highest folder (course-work-2) is also a pre-requisite.

 The code was programmed and run on Python 3. It assumes that pip is installed. To setup all dependancies please ensure this is installed then:

  1. Create a virtual environment at the top level of the repository with the terminal command: `python -m venv .venv`.
  2. Install additional pre-requisite software requirements with Pip with the terminal command: `.\.venv\Scripts\python.exe pip install -r requirements.txt`. 
  3. Activate and open the virtual environment with the terminal command: `source .venv/Scripts/activate`.
  4. To run the game, use the terminal command: `python main.py`.
