# Archie created this file and it has been added to and adapted by Yael

import pygame
import numpy as np
from loguru import logger as log
from pathlib import Path

from custom_exceptions import ManualExit, NoValidGameState
from basic_classes import Button, StateMachineBasic, Textbox


# state is represented as a numpy array where 0 is the an empty space and 1 is a filled space
class GameOfLife():
    def __init__(self, configuration: dict, topLevelDir: Path):
        """Initialises the Game of life

        Args:
            configuration (dict): configuration dictionary
            topLevelDir (Path): directory to consider all paths relative to

        Raises:
            e: error
        """
        try:
            log.info("Initialising game of life object")

            # Initialise basic state machine; see '/state_machine.drawio' for visual representation #
            self.stateMachine = StateMachineBasic({ "start_screen" : "user_input_screen", "user_input_screen" : "game_screen", "game_screen" : "user_input_screen" })

            # Setup PyGame #
            log.info("Initialising start screen")
            pygame.init()
            self.buttonPressed = False  # button pressed record
            self.keyPressFlag = False   # flag indicating key has been pressed
            self.keyEvent = None        # flag indicating the current key event

            # Setup variables #
            self._parse_config(configuration, topLevelDir)

            log.info("Initialising game state")
            self.state = np.zeros(self.worldSize, dtype=int)

            pygame.display.set_caption('Game of Life')
            pygame.display.update()

            # reset mouse click stochastic
            self.mouseClickCount = 0

        except Exception as e:
            raise e

    # coded by Archie
    def iterate(self):
        """Iterates through the simple game of life state machine.
        States are considered to follow a process of:
        1. perform some process (checking user inputs etc.)
        2. render the current screen frame
        3. check for the next state condition

        Raises:
            NoValidGameState: Results from an improperly set state machine
            e: error
        """
        try:
            if self.stateMachine.currentState == "start_screen":            # START SCREN
                self.start_game_screen()                                        # update start screen
                pygame.display.update()                                         # display start screen
                if self.start_button.check_clicked(self.screen, pygame):        # check leave condition
                    self.stateMachine.next_state()
                    self.game_init()                                            # initialise the game space

            elif self.stateMachine.currentState == "user_input_screen":     # USER INPUT SCREEN
                self.user_input()                                               # process screen
                self.game_render()                                              # render game
                if self.ok_button.check_clicked(self.screen, pygame):
                    self.update_rules()                                         # update the numerical rules
                    self.stateMachine.next_state()                              # leave on return value of process

            elif self.stateMachine.currentState == "game_screen":           # GAME SCREEN
                self.game_state_update()                                        # update game state
                self.game_render()                                              # render the game

                if self.reset_button.check_clicked(self.screen, pygame):        # check if the user wants to reset the game settings
                    self.stateMachine.next_state()                              # move to next state of the game

                    self.reset_game()                                           # reset the game values

                if self.ok_button.check_clicked(self.screen, pygame):           # if the user updates the rules add these to the game
                    self.update_rules()

            else:
                raise NoValidGameState(self.stateMachine.currentState)
        except Exception as e:
            raise e

    # coded by Yael
    def game_init(self):
        """Initialises the game space after the start screen, should be run between these states
        """
        # sets default rules for the game
        self.defaultFirstRuleInput = 1
        self.firstRuleInput = 1
        self.defaultSecondRuleInput = 3
        self.secondRuleInput = 3
        self.defaultThirdRuleInput = 3
        self.thirdRuleInput = 3
        self.defaultFourthRuleInput = 3
        self.fourthRuleInput = 3

        #define boundary for user interface panel
        self.uiSeperationWidth = float(self.screenSize[0]) * 0.8

        # creates input textbox instances
        self.input_textbox_1 = Textbox(pygame, 1090, 95, 150, 50)
        self.input_textbox_2 = Textbox(pygame, 1090, 150, 150, 50)
        self.input_textbox_3 = Textbox(pygame, 1090, 205, 150, 50)
        self.input_textbox_4 = Textbox(pygame, 1090, 260, 150, 50)

        # creates button instances
        self.ok_button = Button(1080, 315, self.okayButtonImage)
        self.reset_button = Button(1080, 20, self.resetButtonImage)

        self.generationCount = 0    # persistent generation count


    # written by Archie
    def user_input(self):
        """Gets the user input for that screen including life setting and rule initialisation

        Returns:
            bool: specifies whether the next state should be entered due to the way the 'okay' bnutton is processed
        """
        # check status of textboxes clicked
        if self.keyPressFlag:
            self.input_textbox_1.update_textbox(self.screen, self.keyEvent)
            self.input_textbox_2.update_textbox(self.screen, self.keyEvent)
            self.input_textbox_3.update_textbox(self.screen, self.keyEvent)
            self.input_textbox_4.update_textbox(self.screen, self.keyEvent)

            self.keyPressFlag = False

        # get if mouse is currently pressed
        if self._mouse_clicked_stochastic():
            position = pygame.mouse.get_pos()
            squareSize = self._get_dynamic_square_size()

            index = [0, 0]  # index of life being clicked

            # check horizontal overlap
            foundOverlap = False
            for i_x, px_x in enumerate(range(0, self.worldSize[0] * squareSize, squareSize)):   # for all horozontal squares...
                if position[0] <= px_x: # check overlap
                    foundOverlap = True
                    index[0] = i_x -1      # set horozontal index
                    break

            # now find vertical offset <- due to the nature of the display this will always exist
            if foundOverlap:
                for i_y, px_y in enumerate(range(0, self.worldSize[1] * squareSize, squareSize)):   # for all vertical squares...
                    if position[1] <= px_y: # check overlap
                        index[1] = i_y-1      # set vertical index
                        break

                # toggle state
                if self.state[index[0], index[1]] == 0:
                    self.state[index[0], index[1]] = 1
                else:
                    self.state[index[0], index[1]] = 0

    # coded by Yael & optimised by Yael out of functions into state machine code
    def update_rules(self):
        # gets the current value of the textboxes
        validation_text_1 = self.input_textbox_1.get_text()
        validation_text_2 = self.input_textbox_2.get_text()
        validation_text_3 = self.input_textbox_3.get_text()
        validation_text_4 = self.input_textbox_4.get_text()

        self.firstRuleInput = self._numerical_validation(validation_text_1,1)
        self.secondRuleInput = self._numerical_validation(validation_text_2,2)
        self.thirdRuleInput = self._numerical_validation(validation_text_3,3)
        self.fourthRuleInput = self._numerical_validation(validation_text_4,4)

    # coded by Archie
    # exit for the user to use
    def exit(self):
        log.info("Exiting...")
        pygame.quit()

    # coded by Archie
    def pygame_event_poll(self):
        try:
            # check if manual exit has been triggered
            for event in pygame.event.get():
                if event.type == pygame.KEYDOWN:
                    self.keyPressFlag = True
                    self.keyEvent = event
                if event.type == pygame.QUIT:               # quit on manual exit
                    raise ManualExit
        except Exception as e:
            raise e

    # function created by Archie, adaptions for the user interaction by Yael
    # iterates through a pygame state update
    def game_state_update(self):
        """Updates the game state, this includes expanding the borders and logging the results for post processing
        The basic rules of the game of life are as follows:
            next state is based on the status of all the neighbouring cells
            The rules are as follows
              - alive with 1/0 neighbours dies from underpopulation
              - alive with 2/3 neighbours survives
              - alive with more than three live neighbours dies from overpopulation
              - dead cells with three live neighbours becomes alive
            by casting a 3x3 window on each cell then summing these values can be condensed to easily comparable values dictating if the cell is alive in the next state
              - if alive
                  sum <= 1  then cell dies
                  sum >= 2 or sum <= 3 then cell survives
                  sum > 3 then cell dies
              - if dead
                  sum == 3 then cell is alive

        However, the user has the option to update any of the numbers within the rules to a new choice of number from 0 to 8.
        """
        try:
            # introduce dummy state, padded by +4 on axis from world size to match the padded state
            dummyState = np.zeros((self.worldSize[0] + 4, self.worldSize[1] + 4), dtype=int)

            # double pad state with dead cells (0)
            statePadded = np.pad(self.state, [(2, 2), (2, 2)], mode = 'constant', constant_values = 0)

            # iterate through entire state & the first additional padding
            expandedBordersFlag = False
            for i_x in range(1, statePadded.shape[0]):
                for i_y in range(1, statePadded.shape[1]):
                    # sum all neighbours
                    cell = statePadded[i_x][i_y]
                    sum = np.sum(statePadded[i_x-1:i_x+2, i_y-1:i_y+2]) - cell

                    # rules written by Archie & were adapted by Yael
                    # check alive -> 1 is alive, 0 is dead
                    # default rules or user input rules

                    if cell == 1:
                        if sum <= self.firstRuleInput:
                            dummyState[i_x][i_y] = 0
                        elif sum <= self.secondRuleInput:
                            dummyState[i_x][i_y] = 1
                        elif sum > self.thirdRuleInput:
                            dummyState[i_x][i_y] = 0
                    else:
                        if sum == self.fourthRuleInput:
                            dummyState[i_x][i_y] = 1

                            # if a dead state in the first set of additional padding becomes alive then the placespace must be expanded
                            # this only happens once so set flag to indicate it
                            if expandedBordersFlag == False and (i_x == 1 or i_x == statePadded.shape[0]-2 or i_y == 1 or i_y == statePadded.shape[1]-2):
                                expandedBordersFlag = True
                                self.worldSize = (self.worldSize[0] + 2, self.worldSize[1] + 2)

            # update state with dummy state
            if expandedBordersFlag:
                self.state = dummyState[1:-1, 1:-1]
            else:
                self.state = dummyState[2:-2, 2:-2]

            # update generation count if alive squares exist
            populationCount = len(np.argwhere(self.state == 1))
            if populationCount:
                self.generationCount += 1

                # update logging information if running (i.e. generation count is not zero)
                if self.generationCount == 1:
                    openType = 'w'      # overwrite at start of operation
                else:
                    openType = 'a'      # append for all instances past this

                with open(self.loggingPath, openType) as file:
                    file.write(f'\n{self.generationCount},{populationCount}')
        except Exception as e:
            raise e

    # original states/try-catch made by Archie, optimised into a function by Yael
    # Archie updated to correct world size not just visually which was causing jitter, this also made it more concise
    # reusable reset state function
    def reset_game(self):
        # reset world size
        self.worldSize = self.defaultWorldSize

        # set state back to original
        self.state = np.zeros(self.worldSize, dtype=int)

        # reset generation count
        self.generationCount = 0

        self.firstRuleInput = self.defaultFirstRuleInput
        self.secondRuleInput = self.defaultSecondRuleInput
        self.thirdRuleInput = self.defaultThirdRuleInput
        self.fourthRuleInput = self.defaultFourthRuleInput

    # Displays the game start screen
    def start_game_screen(self):
        GREEN = (0,255,0)

        self._add_text("Game of Life", (350, 150), colour = GREEN, fontSize=40, fontStyle="arialblack")
        self._add_text("Rules", (350, 225), colour = GREEN)
        self._add_text("1. A cell alive with 1/0 neighbours dies from underpopulation.", (350, 270), colour = GREEN)
        self._add_text("2. A cell alive with 2/3 neighbours survives.", (350, 310), colour = GREEN)
        self._add_text("3. A cell alive more than three live neighbours dies from overpopulation.", (350, 350), colour = GREEN)
        self._add_text("4. A dead cell with three live neighbours becomes alive.", (350, 390), colour = GREEN)
        self._add_text("5. You can select the configuration of the pattern by clicking on cells to highlight them.", (350, 430), colour = GREEN)
        self._add_text("6. Change the numbers in these rules by inputting 0-8 into each textbox by holding mouse over box.", (350, 480), colour = GREEN)
        self._add_text("7. Click the 'Ok' button to start the simulations.", (350, 520), colour = GREEN)
        self._add_text("8. Click the 'Start' button when you are ready to move onto the game!", (350, 560), colour = GREEN)

        #drawing start button
        self.start_button = Button(500, 600, self.startButtonImage)
        self.start_button.draw_button(self.screen, pygame)

    # renders the game state
    def game_render(self):
        # draw background colour #
        self.screen.fill(self.gameBackgroundColour)

        # draw squares #
        # calculate size of squares
        # gives 80% width and 100% height to the world and displays all squares at once
        self.uiSeperationWidth = float(self.screenSize[0]) * 0.8

        squareSize = self._get_dynamic_square_size()

        aliveSquares = np.argwhere(self.state == 1)        # pull alive squares from state
        for coord in aliveSquares:
            coord = tuple(coord * squareSize)
            pygame.draw.rect(self.screen, self.squareColour, (coord, (squareSize, squareSize)))   # draw all alive squares on screen

        # draw square separation
        # draws vertical lines
        offset = squareSize
        while offset < self.uiSeperationWidth:
            self._draw_vertical_line(offset, self.squareSeperatingLineColour)
            offset += squareSize

        # draws horizontal lines
        offset = squareSize
        while offset < self.screenSize[1]:
            self._draw_horizontal_line(offset, self.squareSeperatingLineColour)
            offset += squareSize

        # draw dividing line #
        self._draw_vertical_line(self.uiSeperationWidth, self.guiLineColour, 2)

        # this part is coded by Yael
        #drawing buttons
        self.ok_button.draw_button(self.screen, pygame)
        self.reset_button.draw_button(self.screen, pygame)

        #drawing input textboxes
        self._draw_all_textboxes()

        # draw population and generation text boxes #
        self._add_text(f'Generation Count: {self.generationCount}', (self.uiSeperationWidth + 10, 400))
        self._add_text(f'Population Count: {len(aliveSquares)}', (self.uiSeperationWidth + 10, 440))

        # show display
        pygame.display.flip()


    # Private-ish functions #
    # Util #
    # coded by Archie Crew-Gee & added to by Yael #
    # abstracts the config usage away from the developer
    def _parse_config(self, config: dict, topLevelDir: Path):
        try:
            # Geometric parameters
            self.screenSize = (config["visuals"]["screen"]["size"]["width"], config["visuals"]["screen"]["size"]["height"])
            self.worldSize = (config["game"]["world_size"]["width"], config["game"]["world_size"]["height"])
            self.defaultWorldSize = self.worldSize

            # visuals
            self.squareColour = config["game"]["life_visuals"]["colour"]
            self.squareSeperatingLineColour = config["game"]["life_visuals"]["seperating_line"]
            self.gameBackgroundColour = config["visuals"]["gui"]["game_background_colour"]
            self.guiLineColour = config["visuals"]["gui"]["line_colour"]

            # Setup screen #
            self.screen = pygame.display.set_mode(self.screenSize)

            # logging file
            self.loggingPath = str(topLevelDir / config["log"]["log_file"])

            # button images
            self.startButtonImage = pygame.image.load(str(topLevelDir / config["visuals"]["gui"]["image_relative_paths"]["start_button"])).convert_alpha()
            self.okayButtonImage = pygame.image.load(str(topLevelDir / config["visuals"]["gui"]["image_relative_paths"]["okay_button"])).convert_alpha()
            self.resetButtonImage = pygame.image.load(str(topLevelDir / config["visuals"]["gui"]["image_relative_paths"]["reset_button"])).convert_alpha()
        except KeyError as e:
            log.error("Invalid configuration")
            raise e
        except Exception as e:
            raise e

    # get square size to fit world within 80 % of the screen
    def _get_dynamic_square_size(self):
        worldExpansionFlagHeight = 0
        worldExpansionFlagWidth = 0

        # Yael fixed the bug
        # check whether there are any alive squares on the border of grid
        for i in range(0,self.worldSize[0]-1):
            if self.state[i][self.worldSize[1]-24] or self.state[i][self.worldSize[1]-25] or self.state[i][self.worldSize[1]-26]:
                worldExpansionFlagHeight = 1

        for j in range(0,self.worldSize[1]-1):
            if self.state[self.worldSize[0]-24][j] or self.state[self.worldSize[0]-25][j] or self.state[self.worldSize[0]-26][j]:
                worldExpansionFlagWidth = 1

        squareSize = int(float(self.screenSize[0]) * 0.8 / float(self.worldSize[0]))

        # Archie coded original and fixed by Yael
        if worldExpansionFlagHeight:
            squareSize = int(float(self.screenSize[1]) * 0.8 / float(self.worldSize[1]))

        if worldExpansionFlagWidth:
            secondSquareSize = int(float(self.screenSize[0]) * 0.8 / float(self.worldSize[0]))

            if squareSize > secondSquareSize:
                squareSize = secondSquareSize

        return squareSize


    # Rendering assistance #
    #line functions coded by Archie
    def _draw_vertical_line(self, offset, colour = "black", width = 1):
        pygame.draw.line(self.screen, colour, (offset, 0), (offset, self.screenSize[1]), width)

    def _draw_horizontal_line(self, offset, colour = "black", width = 1):
        pygame.draw.line(self.screen, colour, (0, offset), (float(self.screenSize[0]) * 0.8, offset), width)

    # text addition coded by Yael
    def _add_text(self, text:str, position:tuple, fontStyle:str = "arial", fontSize:int = 20, colour:tuple = (255,255,255)):
        log.info(f'Adding text: "{text}"')
        font = pygame.font.SysFont(fontStyle, fontSize)
        self.screen.blit(font.render(text, True, colour), position)

    # textbox validation function coded by Yael
    def _numerical_validation(self, text, flag):
        log.info(f'Validating user input...')
        if (text.isdigit() and int(text) >=0 and int(text) <=8):
            log.info(f'Valid user input accepted')
            return int(text)
        elif text != '':
            log.info(f'Please input a number between 0-8, rather than a string. This rule will not be implemented in this round.')

        if flag == 1:
            return self.defaultFirstRuleInput
        elif flag == 2:
            return self.defaultSecondRuleInput
        elif flag == 3:
            return self.defaultThirdRuleInput
        else:
            return self.defaultFourthRuleInput

    # coded by Yael
    def _draw_all_textboxes(self):
        self._add_text("1:", (self.uiSeperationWidth + 30, 105), fontStyle = 'arialblack', colour = 'black')
        self.input_textbox_1.draw_textbox(self.screen)
        self._add_text("2:", (self.uiSeperationWidth + 30, 160), fontStyle = 'arialblack', colour = 'black')
        self.input_textbox_2.draw_textbox(self.screen)
        self._add_text("3:", (self.uiSeperationWidth + 30, 215), fontStyle = 'arialblack', colour = 'black')
        self.input_textbox_3.draw_textbox(self.screen)
        self._add_text("4:", (self.uiSeperationWidth + 30, 270), fontStyle = 'arialblack', colour = 'black')
        self.input_textbox_4.draw_textbox(self.screen)


    # written by Archie Crew-Gee
    def _mouse_clicked_stochastic(self):
        """Stochastic to tell if the mouse has been clicked, stops 'bouncing' of input due to polling
        """
        if pygame.mouse.get_pressed()[0]:
            self.mouseClickCount += 1
        if self.mouseClickCount > 3:
            self.mouseClickCount = 0
            return True

        return False
