## These files have coder contribution specified but design discussion
#  and code split was discussed and agreed with both authors.

# Original file written by Archie, added to by Yael
import json                         # for loading configuration file
from pathlib import Path            # system agnostic path handling module
from loguru import logger as log    # logging module

from game_of_life import GameOfLife
from basic_classes import FrameSync

# constant-ish #
parentDirectory = Path(__file__).parent

# begin #
log.info("Begin...")
game = None
frameSync = None

try:
    # load config and initialise class #
    with open(parentDirectory / "config.json") as file:
        config = json.load(file)                                    # load config
        frameSync = FrameSync(float(config["game"]["framerate"]))   # load frame sync class
        game = GameOfLife(config, parentDirectory)                  # load game of life class

    # run through iterable game of life
    while True:
        game.pygame_event_poll()
        game.iterate()

        frameSync.wait_frame()      # wait for framerate to have elapsed; framerate management is agnositc of the game engine

except Exception as e:
    if game != None: game.exit()
    raise e
