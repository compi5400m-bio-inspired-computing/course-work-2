from time import time_ns    # time keeping module

from custom_exceptions import InvalidState

# Created by Archie Crew-Gee #
# class for syncing the framerate based off a given period in seconds (float)
class FrameSync:
    def __init__(self, frameRate_s: float):
        self.framePeriod_ns = int(1000000000.0 / frameRate_s)   # calculate period of framerate
        self.previousTime = time_ns()

    def wait_frame(self):
        # wait until correct time has elapsed (this may be instantaineous)
        while (time_ns() - self.previousTime < self.framePeriod_ns):
            pass

        self.previousTime = time_ns()
        #function now exits at the correct time

# Created by Archie Crew-Gee #
# basic state machine where the only a next state item can be performed
# state machine
class StateMachineBasic():
    def __init__(self, stateMachineRepresentation: dict, startState: str = None):
        try:
            self.stateMachine = stateMachineRepresentation

            if startState == None:
                self.currentState = list(self.stateMachine.keys())[0]     # default to first state in representation
            else:
                self.currentState = startState
        except KeyError:
            raise InvalidState
        except Exception as e:
            raise e

    # updates the state machine and returns the new state
    def next_state(self):
        try:
            self.currentState = self.stateMachine[self.currentState]
            return self.currentState
        except KeyError:
            raise InvalidState(self.currentState)
        except Exception as e:
            raise e

## Class designed by Yael May
# class for making button on the screen
class Button():
  def __init__(self, x, y, img):
    self.img = img
    self.rect = self.img.get_rect()
    self.rect.topleft = (x, y)

  def draw_button(self, screen, pygame):
    screen.blit(self.img, (self.rect.x, self.rect.y))

  def check_clicked(self, screen, pygame):

    mouse_pos = pygame.mouse.get_pos()

    #check button has been clicked and mouse is on the button
    if self.rect.collidepoint(mouse_pos) and pygame.mouse.get_pressed()[0]:
        return True
    else:
        return False

## Class designed by Yael May
#class for making button on the screen
class Textbox():
     def __init__(self, pygame, x, y, w, h):
       self.pygame = pygame

       # font defined
       self.base_font = self.pygame.font.Font(None, 50)
       self.input_text = ''

       # defining the size and positioning of the textbox
       self.input_textbox = self.pygame.Rect(x, y, w, h)

       # define the font of the textbox
       self.textbox_surface = self.base_font.render(self.input_text, True, (0, 0, 0))


     def draw_textbox(self, screen):
       # draw rectangle on screen and update screen
       self.pygame.draw.rect(screen, 'light green', self.input_textbox)
       screen.blit(self.textbox_surface, (self.input_textbox.x, self.input_textbox.y))

       # set width limit of input text
       self.input_textbox.w = max(150, self.textbox_surface.get_width())

     def update_textbox(self, screen, event):
       mouse_pos = self.pygame.mouse.get_pos()

       #check that textbox has been checked
       if self.input_textbox.collidepoint(mouse_pos):
            # Check for backspace
            if event.key == self.pygame.K_BACKSPACE:
                # removes last letter from textbox string
                self.input_text = self.input_text[:-1]

            # Adds typed character to textbox
            else:
                self.input_text += event.unicode

            # update the textbox surface with the current input text
            self.textbox_surface = self.base_font.render(self.input_text, True, (0, 0, 0))

            # draw the textbox and update the screen
            self.draw_textbox(screen)

     def get_text(self):
         return self.input_text
