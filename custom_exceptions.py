from loguru import logger as log

# Created by Archie Crew-Gee #
class ManualExit(Exception):
    def __init__(self):
        log.error("Manual exit pressed")

# Created by Archie Crew-Gee #
class InvalidState(Exception):
    def __init__(self, state):
        log.error(f'{state} is an invalid state and cannot be reached')

# Created by Archie Crew-Gee #
class NoValidGameState(Exception):
    def __init__(self, state):
        log.error(f'{state} is not a valid game state')
